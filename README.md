# Open Sim-One开源项目
## 概述
[51Sim-One自动驾驶仿真测试平台](https://www.51aes.com/technology/smart-car)集道路与场景环境仿真、交通流与智能体仿真、多传感器仿真、车辆动力学仿真、并行加速计算等功能于一体。功能模块覆盖自动驾驶仿真测试的全流程，具有大规模、高精度和高真实感等特点，可用于自动驾驶感知、决策、控制算法的测试和训练。

Open Sim-One是51WORLD旗下仿真平台51Sim-One的开源版本。51Sim-One是51WORLD历时四年打造的国内自动驾驶仿真测试领域的知名头部平台。基于Open Sim-One以及51Sim-One社区版相关模块，用户将可以自行搭建定制化仿真平台，解决不同行业及不同方向的仿真应用问题。
## 构建
* 代码获取:
	```
	git clone https://gitee.com/OpenSimOne/OpenSimOne.git
	cd OpenSimOne
	git submodule update --init --recursive
	```

## 整体架构
![Open Sim-One架构图](Docs/images/SimOneArchitecture917.png)

## 子模块
* [Sim-One 仿真平台内核模块 SimOneCore](https://gitee.com/OpenSimOne/SimOneCore)
* [Sim-One 仿真平台API模块 SimOneAPI](https://gitee.com/OpenSimOne/SimOneAPI)

## 社区
  在使用过程中遇到问题，或有好的意见建议，请提交[Issues](https://gitee.com/OpenSimOne/OpenSimOne/issues)。<br/>

  欢迎贡献代码，提交[Pull Requests](https://gitee.com/OpenSimOne/OpenSimOne/pulls)。
## 贡献
## 版权
北京五一视界 版权所有 © Copyright 2021 [51aes.com](https://www.51aes.com/), Inc.


许可证遵循 [BSD 协议]. 更多细节请访问 [LICENSE](https://gitee.com/OpenSimOne/OpenSimOne/blob/master/LICENSE ).

## 鸣谢
## 其他

