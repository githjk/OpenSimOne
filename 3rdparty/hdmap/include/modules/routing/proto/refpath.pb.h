// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: routing/proto/refpath.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_routing_2fproto_2frefpath_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_routing_2fproto_2frefpath_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3012000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3012003 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
#include "common/proto/header.pb.h"
#include "common/proto/geometry.pb.h"
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_routing_2fproto_2frefpath_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_routing_2fproto_2frefpath_2eproto {
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::AuxiliaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::ParseTable schema[1]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::PROTOBUF_NAMESPACE_ID::internal::FieldMetadata field_metadata[];
  static const ::PROTOBUF_NAMESPACE_ID::internal::SerializationTable serialization_table[];
  static const ::PROTOBUF_NAMESPACE_ID::uint32 offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_routing_2fproto_2frefpath_2eproto;
namespace rt {
namespace routing {
class Refpath;
class RefpathDefaultTypeInternal;
extern RefpathDefaultTypeInternal _Refpath_default_instance_;
}  // namespace routing
}  // namespace rt
PROTOBUF_NAMESPACE_OPEN
template<> ::rt::routing::Refpath* Arena::CreateMaybeMessage<::rt::routing::Refpath>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace rt {
namespace routing {

// ===================================================================

class Refpath PROTOBUF_FINAL :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:rt.routing.Refpath) */ {
 public:
  inline Refpath() : Refpath(nullptr) {};
  virtual ~Refpath();

  Refpath(const Refpath& from);
  Refpath(Refpath&& from) noexcept
    : Refpath() {
    *this = ::std::move(from);
  }

  inline Refpath& operator=(const Refpath& from) {
    CopyFrom(from);
    return *this;
  }
  inline Refpath& operator=(Refpath&& from) noexcept {
    if (GetArena() == from.GetArena()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return GetMetadataStatic().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return GetMetadataStatic().reflection;
  }
  static const Refpath& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const Refpath* internal_default_instance() {
    return reinterpret_cast<const Refpath*>(
               &_Refpath_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(Refpath& a, Refpath& b) {
    a.Swap(&b);
  }
  inline void Swap(Refpath* other) {
    if (other == this) return;
    if (GetArena() == other->GetArena()) {
      InternalSwap(other);
    } else {
      ::PROTOBUF_NAMESPACE_ID::internal::GenericSwap(this, other);
    }
  }
  void UnsafeArenaSwap(Refpath* other) {
    if (other == this) return;
    GOOGLE_DCHECK(GetArena() == other->GetArena());
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  inline Refpath* New() const final {
    return CreateMaybeMessage<Refpath>(nullptr);
  }

  Refpath* New(::PROTOBUF_NAMESPACE_ID::Arena* arena) const final {
    return CreateMaybeMessage<Refpath>(arena);
  }
  void CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) final;
  void CopyFrom(const Refpath& from);
  void MergeFrom(const Refpath& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  ::PROTOBUF_NAMESPACE_ID::uint8* _InternalSerialize(
      ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  inline void SharedCtor();
  inline void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(Refpath* other);
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "rt.routing.Refpath";
  }
  protected:
  explicit Refpath(::PROTOBUF_NAMESPACE_ID::Arena* arena);
  private:
  static void ArenaDtor(void* object);
  inline void RegisterArenaDtor(::PROTOBUF_NAMESPACE_ID::Arena* arena);
  public:

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;
  private:
  static ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadataStatic() {
    ::PROTOBUF_NAMESPACE_ID::internal::AssignDescriptors(&::descriptor_table_routing_2fproto_2frefpath_2eproto);
    return ::descriptor_table_routing_2fproto_2frefpath_2eproto.file_level_metadata[kIndexInFileMessages];
  }

  public:

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kPointsFieldNumber = 2,
    kHeaderFieldNumber = 1,
  };
  // repeated .rt.common.PointENU points = 2;
  int points_size() const;
  private:
  int _internal_points_size() const;
  public:
  void clear_points();
  ::rt::common::PointENU* mutable_points(int index);
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::rt::common::PointENU >*
      mutable_points();
  private:
  const ::rt::common::PointENU& _internal_points(int index) const;
  ::rt::common::PointENU* _internal_add_points();
  public:
  const ::rt::common::PointENU& points(int index) const;
  ::rt::common::PointENU* add_points();
  const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::rt::common::PointENU >&
      points() const;

  // .rt.common.Header header = 1;
  bool has_header() const;
  private:
  bool _internal_has_header() const;
  public:
  void clear_header();
  const ::rt::common::Header& header() const;
  ::rt::common::Header* release_header();
  ::rt::common::Header* mutable_header();
  void set_allocated_header(::rt::common::Header* header);
  private:
  const ::rt::common::Header& _internal_header() const;
  ::rt::common::Header* _internal_mutable_header();
  public:
  void unsafe_arena_set_allocated_header(
      ::rt::common::Header* header);
  ::rt::common::Header* unsafe_arena_release_header();

  // @@protoc_insertion_point(class_scope:rt.routing.Refpath)
 private:
  class _Internal;

  template <typename T> friend class ::PROTOBUF_NAMESPACE_ID::Arena::InternalHelper;
  typedef void InternalArenaConstructable_;
  typedef void DestructorSkippable_;
  ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::rt::common::PointENU > points_;
  ::rt::common::Header* header_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  friend struct ::TableStruct_routing_2fproto_2frefpath_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// Refpath

// .rt.common.Header header = 1;
inline bool Refpath::_internal_has_header() const {
  return this != internal_default_instance() && header_ != nullptr;
}
inline bool Refpath::has_header() const {
  return _internal_has_header();
}
inline const ::rt::common::Header& Refpath::_internal_header() const {
  const ::rt::common::Header* p = header_;
  return p != nullptr ? *p : *reinterpret_cast<const ::rt::common::Header*>(
      &::rt::common::_Header_default_instance_);
}
inline const ::rt::common::Header& Refpath::header() const {
  // @@protoc_insertion_point(field_get:rt.routing.Refpath.header)
  return _internal_header();
}
inline void Refpath::unsafe_arena_set_allocated_header(
    ::rt::common::Header* header) {
  if (GetArena() == nullptr) {
    delete reinterpret_cast<::PROTOBUF_NAMESPACE_ID::MessageLite*>(header_);
  }
  header_ = header;
  if (header) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_unsafe_arena_set_allocated:rt.routing.Refpath.header)
}
inline ::rt::common::Header* Refpath::release_header() {
  
  ::rt::common::Header* temp = header_;
  header_ = nullptr;
  if (GetArena() != nullptr) {
    temp = ::PROTOBUF_NAMESPACE_ID::internal::DuplicateIfNonNull(temp);
  }
  return temp;
}
inline ::rt::common::Header* Refpath::unsafe_arena_release_header() {
  // @@protoc_insertion_point(field_release:rt.routing.Refpath.header)
  
  ::rt::common::Header* temp = header_;
  header_ = nullptr;
  return temp;
}
inline ::rt::common::Header* Refpath::_internal_mutable_header() {
  
  if (header_ == nullptr) {
    auto* p = CreateMaybeMessage<::rt::common::Header>(GetArena());
    header_ = p;
  }
  return header_;
}
inline ::rt::common::Header* Refpath::mutable_header() {
  // @@protoc_insertion_point(field_mutable:rt.routing.Refpath.header)
  return _internal_mutable_header();
}
inline void Refpath::set_allocated_header(::rt::common::Header* header) {
  ::PROTOBUF_NAMESPACE_ID::Arena* message_arena = GetArena();
  if (message_arena == nullptr) {
    delete reinterpret_cast< ::PROTOBUF_NAMESPACE_ID::MessageLite*>(header_);
  }
  if (header) {
    ::PROTOBUF_NAMESPACE_ID::Arena* submessage_arena =
      reinterpret_cast<::PROTOBUF_NAMESPACE_ID::MessageLite*>(header)->GetArena();
    if (message_arena != submessage_arena) {
      header = ::PROTOBUF_NAMESPACE_ID::internal::GetOwnedMessage(
          message_arena, header, submessage_arena);
    }
    
  } else {
    
  }
  header_ = header;
  // @@protoc_insertion_point(field_set_allocated:rt.routing.Refpath.header)
}

// repeated .rt.common.PointENU points = 2;
inline int Refpath::_internal_points_size() const {
  return points_.size();
}
inline int Refpath::points_size() const {
  return _internal_points_size();
}
inline ::rt::common::PointENU* Refpath::mutable_points(int index) {
  // @@protoc_insertion_point(field_mutable:rt.routing.Refpath.points)
  return points_.Mutable(index);
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::rt::common::PointENU >*
Refpath::mutable_points() {
  // @@protoc_insertion_point(field_mutable_list:rt.routing.Refpath.points)
  return &points_;
}
inline const ::rt::common::PointENU& Refpath::_internal_points(int index) const {
  return points_.Get(index);
}
inline const ::rt::common::PointENU& Refpath::points(int index) const {
  // @@protoc_insertion_point(field_get:rt.routing.Refpath.points)
  return _internal_points(index);
}
inline ::rt::common::PointENU* Refpath::_internal_add_points() {
  return points_.Add();
}
inline ::rt::common::PointENU* Refpath::add_points() {
  // @@protoc_insertion_point(field_add:rt.routing.Refpath.points)
  return _internal_add_points();
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedPtrField< ::rt::common::PointENU >&
Refpath::points() const {
  // @@protoc_insertion_point(field_list:rt.routing.Refpath.points)
  return points_;
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace routing
}  // namespace rt

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_routing_2fproto_2frefpath_2eproto
